\documentclass[../main.tex]{subfiles}

\begin{document}
\subsection{Introducción a TXAC}
\label{sec:txacintro}
La plataforma de TXAC ha sido desarrollada de manera independiente a la realización de este trabajo. A continuación se describirán sus características y funcionamiento, con el objetivo de establecer el marco de desarrollo del módulo de enseñanza en IA.

Los cursos de TXAC se componen de una serie de tareas propuestas para que el alumno las resuelva mediante código. Los programas que se introducen como soluciones para cada tarea son traducidos a una secuencia de movimientos por parte de un avatar dentro de un entorno gráfico con disposición de cuadrícula.
\subsubsection{Interfaz}
Como se muestra en la figura \ref{fig:interfaz1}, la interfaz de TXAC está dividida en tres partes principales: 1) arriba a la izquierda, la ventana del enunciado y los mensajes, 2) abajo a la izquierda, la interfaz gráfica del entorno - que en adelante llamaremos \strong{mundo de Roby} - y 3) a la derecha, el editor de código. En la ventana del enunciado aparece la descripción de la tarea, típicamente la definición del objetivo del programa que debe escribir el alumno. El editor de código ofrece resaltado sintáctico y numeración de líneas. El botón \textit{Run}, situado sobre la ventana del enunciado, inicia la ejecución el programa, haciendo que el avatar se mueva según las instrucciones del usuario e imprime la salida en la pestaña de mensajes, que adquiere el foco automáticamente, como se ve en las figuras \ref{fig:interfaz2} y \ref{fig:interfaz3}. Los botones de \textit{Tips} y \textit{Wiki} abren ventanas emergentes con pistas e información útil relacionada con la tarea. En caso de que la tarea se resuelva con éxito, se muestra un mensaje de felicitación y un botón para pasar a la siguiente tarea (ver fig. \ref{fig:interfaz2}), si la hay. En caso contrario, se muestra un mensaje de error y un botón para reiniciar el mundo de Roby (ver fig. \ref{fig:interfaz3}).

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{images/interfaz1.png}
	\caption{Interfaz de ToolboX.Academy antes de ejecutar el código del alumno}
	\label{fig:interfaz1}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{images/interfaz2.png}
	\caption{Interfaz de ToolboX.Academy después de ejecutar el código del alumno}
	\label{fig:interfaz2}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{images/interfaz3.png}
\caption{Interfaz de ToolboX.Academy después de ejecutar el código del alumno}
\label{fig:interfaz3}
\end{figure}

\subsubsection{Lenguajes de programación}
Actualmente TXAC cuenta con soporte para dos lenguajes de programación en sus tareas: ToyScript, un lenguaje específico de la plataforma \parencite[pp. 22-23]{perez2020propuesta}, y Javascript. El primero, al ser un lenguaje diseñado para la enseñanza de conceptos fundamentales de programación, tiene ciertas restricciones de diseño, como la ausencia de subrutinas y de tipos complejos, que obstaculizaría el desarrollo de algoritmos más avanzados. Por lo tanto, las tareas propuestas en este trabajo están pensadas para cursos en Javascript. Esto ha facilitado notablemente la integración de las librerías en la aplicación web. 

\subsubsection{Mundo de Roby}
\label{sec:mundoderoby}
El entorno gráfico dónde se mueve el avatar tiene la temática de una caricaturesca fábrica de robots, para resultar amigable y atractiva. El agente principal que ejecuta el código del alumno es Roby, un pequeño robot que cumple tareas que le asignan Tor y Eva, los otros robots de la fábrica. Los principales elementos que se encuentra Roby en el entorno son tuercas y tornillos que puede recoger, puertas etiquetadas, cajas y otros robots, que contienen información necesaria para la tarea, y muros que no puede traspasar. En la figura \ref{fig:elements} se muestran estos elementos en la aplicación.

Las reglas del mundo son las siguientes:

\begin{itemize}
	\item Roby sólo puede moverse a casillas que estén dentro de la cuadrícula y en las que no haya un muro (ver fig. \ref{fig:crash}).
	\item Si Roby se mueve a la posición de una tuerca o un tornillo, ésta o éste desaparecen de la cuadrícula y se incrementa un contador interno de elementos recogidos (comparar fig. \ref{fig:interfaz1} y fig. \ref{fig:interfaz2}).
	\item Roby sólo puede obtener información de un robot o una caja si se encuentra en su misma posición (ver fig. \ref{fig:info}).
\end{itemize}
\begin{figure}[p]
	\centering
	\includegraphics[width=0.7\linewidth]{elements.png}
	\caption{De izquierda a derecha, de arriba a abajo: Roby, Tor, Eva, una tuerca, un tornillo, una caja, un muro y dos puertas.}
	\label{fig:elements}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics[width=0.7\linewidth]{crash.png}
\caption{Roby después de intentar moverse hacia un muro.}
\label{fig:crash}
\end{figure}

\begin{figure}[p]
\centering
\begin{subfigure}{0.49\linewidth}
\includegraphics[width=0.97\linewidth]{info1.png}
\caption{Información de Tor.}
\end{subfigure}
\begin{subfigure}{0.49\linewidth}
\includegraphics[width=0.97\linewidth]{info2.png}
\caption{Información de una caja.}
\end{subfigure}
\caption{Roby pidiendo información.}
\label{fig:info}
\end{figure}


\subsection{Análisis formal del mundo de Roby}

El primer requisito para una propuesta de tareas de IA en TXAC es definición formal del entorno, para que sirva de referencia en el estudio de los posibles problemas y soluciones de IA que tiene sentido implementar.

\subsubsection{Definiciones previas}

\begin{definition}[Estado de una casilla]
Definimos el conjunto de estados de una casilla como $$S = \{empty, occupied\left<obj, lab, val\right>, takeable\left<obj, lab, val\right>\}$$
donde $obj$ y $lab$ (objeto y etiqueta) son cadenas de texto y $val$ (valor) un valor numérico.
\end{definition}

\begin{definition}[Entorno de una tarea]
\label{def:entornotarea}
Definimos el entorno de una tarea como una tupla $(R, C, X)$ donde $R \times C$ son las dimensiones de la cuadrícula (filas por columnas) y $X$ es el conjunto de casillas, definidas por su fila, su columna y su estado:$$X = \left\{(i,j,s)\;\middle|\;0 < i \leq R,\,0 < j \leq C, s \in S\right\}$$ 
Debe cumplir que $(i,j,s_1), (i,j,s_2) \in E \Rightarrow s_1 = s_2$; es decir, que una misma casilla no puede tener más de un estado.

\end{definition}

\begin{definition}[Posición bloqueada]
Dado un entorno $(R, C, X)$, el conjunto de posiciones bloqueadas se define como: $$\{(i,j) \;|\; 0 < i \leq R,\, 0 < j \leq C, \nexists s \in S : (i,j,s) \in E\}$$
\noindent Son posiciones válidas en la cuadrícula que no tienen un estado en el entorno.
\end{definition}

\begin{definition}[Inventario]
Definimos un inventario como una función $$I: Str \to \mathbb{N}_0$$ donde $Str$ es, informalmente, el conjunto de las cadenas de texto.
\end{definition}

\begin{definition}[Estado del mundo]
Definimos el estado del mundo como una tupla $(R_r, C_r, I, E)$ donde la posición $(R_r, C_r)$ no está bloqueada e indica dónde se encuentra Roby en la cuadrícula, $I$ es una función inventario y $E$ es el entorno de la tarea..
\end{definition}

\begin{definition}[Estado del agente]
Definimos el estado del agente como el estado interno del programa del usuario. 
\end{definition}

\begin{definition}[Función $take$]
La función $take$ devuelve un estado del mundo modificado, cambiando el estado de una casilla y actualizando la función inventario:
$$take(r,c,i_1,e_1) = \left\{\begin{array}{ll}
	(r,c,i_2,e_2) & \text{si } \exists\, obj, lab, val \;|\; (r,c,takeable\left<obj, lab, val\right>) \in X \\
	(r,c,i_1,e_1) & \text{en otro caso}
\end{array}\right.$$
donde
$$\begin{array}{l}
e_1 = (R, C, X)\\
e_2 = (R, C, (X - \{(r,c,takeable\left<obj, lab, val\right>)\}) \cup \{(r,c,empty)\}) \\
i_2(x) = \left\{\begin{array}{ll}
	i_1(x) + val & \text{si } x = obj \\
	i_1(x) & \text{en otro caso}
\end{array}\right.
\end{array}$$

Se trata de una función que cambia una casilla $takeable$ por una $empty$ - decimos que el objeto de la casilla ha sido recogido - y cambia la función de inventario de manera que devuelva la suma de los valores viejo y nuevo asociados a dicho objeto.

\end{definition}

\begin{definition}[Funcioń $move$]
La función $move$ devuelve un estado del mundo modificado, cambiando la posición de Roby y modificando el entorno y el inventario en caso de que la nueva posición coincida con una casilla $takeable$.

$$move(\Delta r, \Delta c, r_1, c_1, i_1, e_1) = \left\{\begin{array}{ll}
	(r_2, c_2, i_2, e_2) & \text{ si }0 < r_2 < R,\, 0 < c_2 < C,\, \exists\, s \;|\; (r_2, c_2, s) \in X\\
	\bot & \text{en otro caso}
	\end{array}\right.$$
donde
$$\begin{array}{l}
e_1 = (R, C, X)\\
r_2 = r_1 + \Delta r \\
c_2 = c_1 + \Delta c \\
take(r_2, c_2, i_1, e_1) = (r_2, c_2, i_2, e_2)
\end{array}$$

Nótese que al ser una función que puede diverger, queda implícita la existencia de movimientos inválidos por parte Roby: aquellos que conduzcan a una posición bloqueada o fuera de la cuadrícula.

\end{definition}

\subsubsection{Acciones y percepción}
Roby, el agente software, sólo puede modificar el estado del mundo mediante comandos de movimiento, que se traducen a cambios en su posición y en las casillas y el inventario al recoger objetos. Cada uno de los comandos se corresponde en Javascript con una función, parte de la librería base de TXAC.

Usaremos la notación $\left<a, S_1\right> \rightarrow S_2$ para expresar que, si en el estado del mundo $S_1$, el agente lleva a cabo la acción $a$, entonces el nuevo estado del mundo será $S_2$. Nótese que los cambios en el estado del agente no son expresados.

\begin{center}
\begin{tabular}{l|l|l}
	\textbf{Acción} & \textbf{Transición} &  \textbf{Función en Javascript}\\\hline
	\textit{up} & $\left<up, (r,c,i,e)\right> \rightarrow move(-1, 0, r, c, i, e)$ & \texttt{up()}\\
	\textit{left} & $\left<left,(r,c,i,e)\right>\rightarrow move(0, -1, r, c, i, e)$ & \texttt{left()}\\
	\textit{down} & $\left<down,(r,c,i,e)\right>\rightarrow move(1, 0, r, c, i, e)$ & \texttt{down()}\\
	\textit{right} & $\left<right,(r,c,i,e)\right>\rightarrow move(0, 1, r, c, i, e)$ & \texttt{right()}
	
\end{tabular}
\end{center}

Además de las acciones para modificar el estado del mundo, Roby cuenta con una función de percepción, \textit{info}, con la que potencialmente modificar su estado interno. Esta acción puede llevarse a cabo cuando Roby se encuentra en una casilla con estado $occupied\left<obj, lab, val\right>$ para obtener el valor $val$. Si se usa en otro caso, la percepción resulta en un valor fijo de $-1$.

Sea $(r, c, i, e)$ el estado actual de una tarea. Las acciones de percepción que el agente Roby puede llevar a cabo son:

\begin{center}
	\begin{tabular}{l|l|l}
		\textbf{Acción} & \textbf{Información obtenida} &  \textbf{Función en Javascript}\\\hline
		\textit{info} & $\left\{\begin{array}{ll}val & \text{si }\exists\, obj \;|\; (r,c,occupied\left<obj, lab, val\right>) \in e\\-1&\text{en otro caso}\end{array}\right.$ & \texttt{info()}
	
\end{tabular}
\end{center}

\subsubsection{Naturaleza del entorno}

Según las categorías descritas en \textcite[capítulo~2.3]{russellnorvig2010}, podemos clasificar el mundo de Roby como un entorno:
\begin{itemize}
\item \emph{Parcialmente observable}. La percepción de Roby, a través de la acción \textit{info}, se limita a información parcial de la misma casilla en la que Roby se encuentre.
\item \emph{Determinista}. Tras cada acción del agente, el siguiente estado del mundo (si lo hay) está bien definido. No hay componente de incertidumbre.
\item \emph{Secuencial}. Los cambios que Roby efectúan en el mundo pueden afectar a sus decisiones futuras. No hay episodios definidos de percepción-actuación, sino una sola ejecución del programa del usuario, que debe resolver el problema planteado.
\item \emph{Estático}. El entorno no cambia a no ser que Roby actúe.
\item \emph{Discreto}. El conjunto de posibles estados de una tarea es finito.
\item \emph{Conocido}. Las reglas del entorno y el efecto de las acciones del agente se conocen y no varían en ningún momento.
\end{itemize}




%\begin{figure}
%    \centering
    %\includegraphics[width=\textwidth]{images/entorno1.png}
%    \caption{Cuadrícula con casillas en distintos estados. De izquierda a derecha: ocupada por el agente (Roby), ocupada por un agente auxiliar (Tor), ocupada por un objeto no recogible (puerta), ocupada por un objeto recogible (tuerca), bloqueada (muro)}
%    \label{fig:entorno1}
%\end{figure}

\subsection{Estudio de posibles problemas de IA para implementar}
\label{sec:estudio}

A continuación se enumeran los problemas que se han estudiado para implementarse potenciales en tareas de TXAC. Las ilustraciones a continuación sólo son maquetas, porque el planteamiento se hace teniendo en cuenta que cada caso podría requerir de modificaciones en el entorno o en el agente y la plataforma no está preparada aún para implementar algunas tareas que se proponen de ejemplo. Por razones de espacio, no se va a profundizar en estas cuestiones aquí.

\subsubsection{Búsqueda de caminos}
\label{sec:prob:pf}
\paragraph{Camino entre dos celdas}
Roby debe encontrar un camino hasta una casilla objetivo, por ejemplo, para recoger una tuerca, evitando posiciones bloqueadas o marcadas como obstáculos.

\begin{center}
\includegraphics{images/task01}
\captionof{figure}{Ejemplo de tarea para búsqueda  de camino entre dos celdas.}
\label{fig:task01}
\end{center}

\paragraph{Problema del viajante}
Roby debe encontrar el camino más corto para recoger varias tuercas.

\begin{center}
\includegraphics{images/task02}
\captionof{figure}{Ejemplo de tarea sobre el problema del viajante.}
\label{fig:task02}
\end{center}

\subsubsection{Optimización combinatoria}
\label{sec:prob:comb}
\paragraph{Problema de las monedas}
Cada objeto en la cuadrícula tiene un valor asociado. Roby debe minimizar el número de objetos que debe recoger para acumular cierto valor, pero sin pasarse de un límite.
\paragraph{Problema de la mochila}
Cada objeto en la cuadrícula tiene un valor y un peso asociados. Roby debe maximizar el valor de los objetos recogidos, dado un límite de peso.

\begin{center}
	\includegraphics{images/task03}
	\captionof{figure}{Ejemplo de tarea sobre optimización combinatoria, donde el enunciado proporcionaría una correspondencia de peso y valor para cada tuerca.}
	\label{fig:task03}
\end{center}

\subsubsection{Razonamiento}
\label{sec:prob:razon}
\paragraph{Satisfacción de restricciones}
Roby debe modificar la posición de los objetos en la cuadrícula para que sigan una disposición concreta.

\begin{center}
	\includegraphics{images/task04}
	\captionof{figure}{Ejemplo de tarea de satisfacción de restricciones, en la que el alumno debe reordenar las tuercas para que se encuentren junto a la puerta de su color.}
	\label{fig:task04}
\end{center}

\paragraph{Lógica}
Dada una base de conocimiento, Roby debe seleccionar un objeto de entre varios, según las proposiciones lógicas de primer orden que reciba durante la ejecución.

\begin{center}
	\includegraphics{images/task05}
	\captionof{figure}{Ejemplo de tarea de lógica, en la que los agentes auxiliares proporcionarían a Roby predicados sobre los que razonar para decidir que objeto recoger.}
	\label{fig:task05}
\end{center}

\paragraph{Planificación}
Roby debe llegar a una casilla objetivo, pero primero debe desbloquear el camino.

\begin{center}
	\includegraphics{images/task06}
	\captionof{figure}{Ejemplo de tarea de planificación, en la que el orden de las acciones de Roby es importante para llegar a su objetivo, con un nuevo tipo de objeto que en lugar de ser recogible, es empujado en la dirección de movimiento de Roby cuando éste entra en su casilla.}
	\label{fig:task06}
\end{center}

\subsubsection{Aprendizaje automático}
\label{sec:prob:ml}
\paragraph{Clasificación}
Roby debe predecir la clase asociada a una o varias cajas según sus características, en base a un conjunto de datos de entrenamiento que cargue de un agente auxiliar.
\paragraph{Regresión}
Roby debe predecir el valor contenido en una o varias cajas según sus características, en base a un conjunto de datos de entrenamiento que cargue de un agente auxiliar.

\begin{center}
	\includegraphics{images/task07}
	\captionof{figure}{Ejemplo de tarea de regresión, en la que Tor proporciona los datos de entrenamiento y Roby debe predecir el valor $Y$ contenido en cada caja para cada valor $X$ de las etiqueta correspondiente}
	\label{fig:task07}
\end{center}



\subsection{Elección de problemas}

Los problemas elegidos para implementarse son los de \strong{búsqueda de caminos}. 

\subsubsection{Justificación}
\label{sec:justificacion}

Los motivos de esta elección son los siguientes:
\begin{itemize}
	\item Los algoritmos de búsqueda en grafos que se usan para solucionarlos son fáciles de ilustrar en el entorno. El resto de problemas no tiene el mismo componente espacial y los procesos de decisión subyacentes son más difíciles de representar visualmente.
	
	Esto es especialmente importante para introducir al alumno a distintos conceptos básicos de algoritmia como  \emph{estrategia de fuerza bruta}, \emph{estrategia voraz} o \emph{heurístico} de una manera más intuitiva.
	
	\item La búsqueda de caminos proporcionará al alumno funciones para programar agentes que abstraigan la navegación en la cuadrícula. Con ello será más fácil que el alumno se centre en problemas más complejos sin preocuparse por los movimientos concretos de Roby.
	
	\item La única modificación previa, necesaria para la implementación de los algoritmos, es añadir al agente nuevas acciones de percepción, puesto que la búsqueda de caminos requiere de un entorno totalmente observable. En un entorno parcialmente observable, sería necesario como mínimo un proceso previo de exploración para construir el mapa sobre el que realizar la búsqueda. De hecho, como la función actual de percepción está limitada a obtener información de la casilla en la que se encuentra Roby, ni siquiera es posible llevar a cabo dicha exploración, al no poder reconocer las posiciones boqueadas sin realizar movimientos no válidos.
	
	Otras tareas requerirían de modificaciones más significativas en las leyes del entorno o el funcionamiento de la plataforma. 
\end{itemize}

\subsubsection{Planteamiento formal}

\paragraph{Búsqueda en grafos}
Hará falta una representación de la cuadrícula en estructura de grafo. Sobre éste aplicaremos búsqueda en anchura como ejemplo de fuerza bruta, el algoritmo de Dijkstra como alternativa voraz y A* como búsqueda informada por un heurístico.

\begin{definition}[Grafo de la cuadrícula]
	Dado el estado de una tarea (ver definición \ref{def:entornotarea}) $(R_r, C_r, I, (R, C, X) )$, podemos definir el grafo de la cuadrícula como $(N, E)$, donde $N$ es el conjunto de nodos o vértices y $E$ es el de aristas.
	$$N = X \cup \left\{(R_r, R_c, occupied\left<Roby, \epsilon, -1\right>)\right\}$$
	$$E = \left\{(a, b) \;|\; \exists\, m \in M : \left<m, (r_a, c_a, i_1, e_1) \rightarrow (r_b, c_b, i_2, e_2) \right> \right\}$$
	donde $M$ es el conjunto de acciones de movimiento del agente $M = \{up, left, down, right\}$ y $a$ y $b$ son nodos en el grafo: $a,b \in N, a = (r_a, c_a, s_a),\, b = (r_b, c_b, s_b)$.
	
	En otras palabras, para el estado de una tarea, el grafo de la cuadrícula contiene la información de las casillas y el agente en los nodos, y las aristas conectan las posiciones que distan una acción de movimiento entre sí (ver figura \ref{fig:grafocuadricula}).
	
	\begin{center}
	\begin{figure}
	\begin{subfigure}{\textwidth}
	\includegraphics[width=0.6\textwidth]{images/task01}
	\caption{Representación visual de la cuadrícula.}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\begin{center}
		\begin{tikzpicture}[every node/.style={shape=circle,draw=black}]
			\node (c11){$e$};
			\node [below = of c11] (c21){$e$};
			\node [below = of c21] (c31) {$e$};
			\node [right = of c11] (c12) {$e$};
			\node [right = of c31] (c32) {$e$};
			\node [right = of c12] (c13) {$r$};
			\node [right = of c32] (c33) {$t$};
			\node [right = of c13] (c14) {$e$};
			\node [right = of c33] (c34) {$e$};
			\node [right = of c14] (c15) {$e$};
			\node [right = of c34] (c35) {$e$};
			\node [right = of c15] (c16) {$e$};
			\node [below = of c16] (c26) {$e$};
			\node [below = of c26] (c36) {$e$};
			
			\matrix [draw=none,rectangle,right = of c26] (legend) {
				\node [draw=none,rectangle] {$e=empty$}; \\
				\node [draw=none,rectangle] {$r=occupied\left<Roby, \epsilon, -1\right>$}; \\
				\node [draw=none,rectangle] {$t=occupied\left<tuerca, \epsilon, 1\right>$}; \\
			};
			
			\path [-] (c11) edge (c12);
			\path [-] (c11) edge (c21);
			\path [-] (c31) edge (c21);
			\path [-] (c31) edge (c32);
			\path [-] (c13) edge (c12);
			\path [-] (c13) edge (c14);
			\path [-] (c33) edge (c32);
			\path [-] (c33) edge (c34);
			\path [-] (c15) edge (c14);
			\path [-] (c15) edge (c16);
			\path [-] (c35) edge (c34);
			\path [-] (c35) edge (c36);
			\path [-] (c26) edge (c16);
			\path [-] (c26) edge (c36);
		\end{tikzpicture}
		\caption{Representación en grafo de la cuadrícula.}
		\end{center}
	\end{subfigure}
	\caption{Ejemplo de la representación en estructura de grafo.}
	\label{fig:grafocuadricula}
	\end{figure}
	\end{center}
	
	
\end{definition}

\begin{definition}[Función \emph{searchpath}] \label{def:searchpath}
	Dado el grafo de una cuadrícula, casilla inicial y una objetivo, la función \emph{searchpath} es aquella que devuelve el vector de nodos que conforman el camino más corto entre las dos casillas.
	$$searchPath((N,E), n_a, n_b) = P$$
	$$n_a,n_b\in N, P \in N^*$$
\end{definition}

\paragraph{Problema del viajante}
Una vez que la navegación en el tablero ha sido abstraída gracias a la búsqueda de caminos, es posible plantear el problema de encontrar la ruta óptima que pase por ciertos puntos clave. Para buscarle solución, necesitamos una nueva representación en grafo de los objetos que queremos recoger. Sobre éste, aplicaremos búsqueda por fuerza bruta, una solución voraz y un algoritmo evolutivo.

\begin{definition}[Mapa de carreteras]
	Dado el grafo de una cuadrícula $(N, E)$, definimos un mapa de carreteras como un una tupla $(C, R, W)$ en el que $C \in N$ es el conjunto de ciudades o vértices del grafo, $R$ es el conjunto de carreteras o aristas y $W$ es la función de peso, qué asigna a cada carretera un peso correspondiente a la longitud del camino mínimo entre las ciudades que conecta.
	$$W(e) = |\,searchpath((N,E),a,b)\,|$$ donde $a,b\in C, e = (a,b) \in R$
	
	En nuestra definición de mapa de carreteras, vamos a excluir carreteras redundantes, imponiendo la siguiente restricción: si existen dos caminos distintos de la misma longitud entre dos ciudades y uno pasa por una tercera ciudad, entonces el conjunto de carreteras no incluye aquella directa entre las dos primeras ciudades. Como ejemplo, en la figura \ref{fig:roadmap}, no hay una carretera directa entre $t_1$ y $t_3$ porque una de la misma longitud existe pasando por $t_2$.
	
	
	\begin{center}
		\begin{figure}
			\begin{subfigure}{0.49\textwidth}
				\begin{center}
					\includegraphics[width=0.8\textwidth]{images/task02}
				\end{center}
				\caption{Representación visual de la cuadrícula.}
			\end{subfigure}
			\begin{subfigure}{0.49\textwidth}
				\begin{center}
					\begin{tikzpicture}[city/.style={shape=circle,draw=black}]
						\node [city] (r1){$r$};
						\node [city,below right = of r1] (t1){$t_1$};
						\node [city,right = of t1] (t2){$t_2$};
						\node [city,above right = of t2] (t3){$t_3$};
						\node [city,above right = of r1] (t4){$t_4$};
						
						\path [-] (r1) edge node[auto] {3} (t1);
						\path [-] (r1) edge node[auto] {2} (t4);
						\path [-] (r1) edge node[auto] {5} (t3);
						\path [-] (t1) edge node[auto] {2} (t2);
						\path [-] (t2) edge node[auto] {2} (t3);
						\path [-] (t3) edge node[above] {5} (t4);
					\end{tikzpicture}
					\caption{Mapa de carreteras.}
				\end{center}
			\end{subfigure}
			\caption{Ejemplo de la representación del mapa de carreteras donde las ciudades son las casillas ocupadas.}
			\label{fig:roadmap}
		\end{figure}
	\end{center}
	
\end{definition}

\begin{definition}[Función \emph{salesman}] \label{def:salesman}
	Llamaremos función \emph{salesman} a aquella que, dado un mapa de carreteras, devuelve un vector con las ciudades en el orden en que deben visitarse para realizar un recorrido óptimo.
	$$salesman(C, R, W) = P$$
	donde $P \in C^{|C|}$
\end{definition}

\paragraph{Nuevas funciones de percepción} \label{newfunper}
El agente va a necesitar: 1) una acción de percepción para encontrar la posición de un objeto único en la cuadrícula, 2) una acción de percepción para reconocer el grafo de la cuadrícula en el estado actual de la tarea y 3) una acción de percepción para construir un mapa de carreteras en el estado actual de la tarea. Las estructuras que estas acciones le permitirán almacenar en su estado interno serán necesarias para poder aplicar sobre ellas los algoritmos que solucionen los problemas planteados. 

\subsection{Prototipo}
Al comienzo de este trabajo, el propio TXAC se encontraba en una etapa incipiente con el nombre de ToolboX, implementado en GNU Octave, y la primera versión del módulo de IA se hizo en este prototipo.
La única función que se implementó fue \texttt{searchAStar}. El entorno era totalmente observable, puesto que ToyScript aún no había sido definido y todo el entorno era observable para el agente a través del estado del programa de Octave. En la figura \ref{fig:proto1} se muestra el aspecto original de ToolboX y en la figura \ref{fig:protosearch}, capturas del proceso ilustrado de búsqueda de caminos mediante el algoritmo A$^*$.

Al comienzo de TXAC, el código del nuevo módulo de IA mantuvo una estructura monolítica, igual que en el prototipo, hasta que se implementó un sistema de carga de librerías. Entonces, el código se separó en diferentes módulos, que se explicarán a continuación.

\begin{figure}
	\includegraphics[width=\textwidth]{images/proto1}
	\caption{Interfaz de ToolboX, predecesor de ToolboX.Academy.}
	\label{fig:proto1}
\end{figure}

\begin{figure}
\begin{subfigure}{0.299\textwidth}
\includegraphics[width=\textwidth]{images/proto2}
\caption{Estado inicial.}
\end{subfigure}
\begin{subfigure}{0.299\textwidth}
\includegraphics[width=\textwidth]{images/proto3}
\caption{Árbol de búsqueda.}
\end{subfigure}
\begin{subfigure}{0.299\textwidth}
\includegraphics[width=\textwidth]{images/proto4}
\caption{Camino encontrado.}
\end{subfigure}

\caption{Proceso de búsqueda de caminos. En azul, casillas correspondientes a nodos abiertos. En amarillo, nodos cerrados. En rojo, nodos parte de la solución.}
\label{fig:protosearch}
\end{figure}

\subsection{Implementación}

\subsubsection{Módulos} 
\begin{description}
	\item [\texttt{Graph.js}]: Implementación de una clase grafo genérica y los algoritmos de búsqueda.
	\item [\texttt{Board.js}]: Funciones relacionadas con el tablero: colorear casillas, pintar conexiones, obtener posiciones de casillas que se ajusten a cierto criterio, funciones de distancia entre casillas...
	\item [\texttt{Evolutive.js}]: Implementación genérica de un algoritmo evolutivo.
	\item [\texttt{Pathfinding.js}]: Funciones para construir el grafo de la cuadrícula, mapas de carreteras, y aplicar búsqueda de caminos o solucionar el problema del viajante en el mundo de Roby.
\end{description}

\subsubsection{Funciones}
\label{sec:implfunciones}
\paragraph{Métodos para renderizar las soluciones}
El principal criterio didáctico en TXAC es que las soluciones deben ser lo más visualmente intuitivas posible. TXAC no contaba con una función para dibujar nada en el tablero, así que fue necesario programar una función \texttt{\textbf{Board.drawConnection}} que renderizase una línea entre dos casillas y un nodo en el centro de cada una. Los colores de cada nodo y el de la conexión se pasan como parámetros de la función. En la figura \ref{fig:drawconnection} se muestra el resultado de llamar a esta función sobre dos casillas contiguas.

A partir de esta función se construyen las siguientes:
\begin{description}
	\item[\texttt{Pathfinding.paintPath}] : Dibuja en orden las conexiones que definen un camino.
	\item[\texttt{Pathfinding.paintSearch}] : Dibuja la evolución del árbol de búsqueda a lo largo del proceso.
	\item[\texttt{Pathfinding.paintRoads}] :  Dibuja los caminos correspondientes a las carreteras del mapa de carreteras, empezando por las que parten de la posición de Roby.
\end{description}

\begin{figure}
	\begin{center}
		\includegraphics[width=0.3\textwidth]{images/drawconnection}
		\caption{Conexión dibujada con nodo origen en rojo, arista en verde y nodo destino en azul.}
		\label{fig:drawconnection}
	\end{center}
\end{figure}

\paragraph{Nuevas funciones de percepción}
Como se ha dicho previamente, el agente necesitaría nuevas acciones de percepción para hacer el entorno totalmente observable. Las funciones que satisfacen esta necesidad son:

\begin{description}
	\item[\texttt{Board.filterCells}] : Devuelve una lista con la información de las casillas que contienen cierto objeto, valor o etiqueta.
	\item[\texttt{Board.findCell}] : Atajo para obtener el primer elemento de la lista devuelta por \texttt{Board.filterCells}. Implementa la primera función de percepción especificada en \ref{newfunper}.
	\item[\texttt{Pathfinding.makeBoardGraph}] : Inicializa una representación interna del grafo de la cuadrícula para el estado actual. Implementa la segunda función de percepción especificada en \ref{newfunper}.
	\item[\texttt{Pathfinding.RoadMap}] : Constructor de un objeto con la información de un mapa de carreteras, para ciertas ciudades. Implementa la tercera función de percepción especificada en \ref{newfunper}.
\end{description}

\paragraph{Algoritmos para las soluciones}
\begin{description}
	\item[\texttt{Pathfinding.searchPath}] : Calcula un camino entre dos posiciones. Pueden especificarse, como parámetros opcionales, qué casillas se consideran obstáculos para que Roby las evite y qué estrategia de búsqueda debe utilizarse - por anchura, por Dijkstra o A$^*$, que utiliza la distancia manhattan como heurístico. Implementa la función \emph{searchpath} (ver definición \ref{def:searchpath}).
	\item[\texttt{Pathfinding.RoadMap.salesmanBrute}] : Calcula el camino óptimo probando todas las permutaciones de las ciudades del mapa. Implementa la función \emph{salesman} (ver definición \ref{def:salesman}).
	\item[\texttt{Pathfinding.RoadMap.salesmanGreedy}] : Calcula un camino no necesariamente óptimo dirigiéndose siempre a la ciudad más cercana. El único motivo por el que no consideramos que implemente \emph{salesman} (ver definición \ref{def:salesman}) es porque no devuelve necesariamente una solución óptima.
	\item[\texttt{Pathfinding.RoadMap.salesmanEvolutive}] : Calcula un camino no necesariamente óptimo aplicando un proceso de evolución sobre una generación inicial aleatoria. Al igual que en el caso anterior, no consideramos que implemente \emph{salesman} (ver definición \ref{def:salesman}) porque puede no devolver una solución óptima.
\end{description}

\subsubsection{Tareas}
Podemos agrupar las tareas de la versión demo del módulo por los conceptos que ilustran:

\paragraph{La misma solución resuelve varios problemas} Los módulos de los cursos existentes enseñan soluciones únicas para una sola configuración de tarea. En cambio, con los algoritmos de IA, el mismo código puede resolver muchas tareas con distintas configuraciones (ver fig. \ref{fig:modulefirstconcept}).

\lstset{language=JavaScript}
\begin{lstlisting}
	import Board
	import Pathfinding
	
	// Define starting point and goal
	start = Board.findCell('Roby')
	goal = Board.findCell('tuerca')
	
	// Perform the search
	result = Pathfinding.searchPath(start, goal)
	
	// Display the process
	Pathfinding.paintSearch(result)
	Pathfinding.paintPath(result, 'successful')
	
	// Make Roby follow the path
	Pathfinding.runPath(result.path)
\end{lstlisting}

\begin{figure}[h]
\begin{subfigure}{\textwidth}
	\begin{center}
 	\includegraphics[width=0.7\textwidth]{images/ia000}
	\end{center}
	\subcaption{Primera tarea.}
\end{subfigure}
\begin{subfigure}{\textwidth}
\begin{center}
 	\includegraphics[width=0.7\textwidth]{images/ia001}
\end{center}
\subcaption{Segunda tarea.}
\end{subfigure}
\begin{subfigure}{\textwidth}
\begin{center}
 	\includegraphics[width=0.7\textwidth]{images/ia002}
\end{center}
\subcaption{Tercera tarea.}
\end{subfigure}
\caption{Tres configuraciones distintas resueltas por el mismo código.}
\label{fig:modulefirstconcept}
\end{figure}

\paragraph{Calidad de la solución y coste de ejecución}
El segundo concepto que se enseña en el módulo es que existen distintos enfoques y que cada uno tiene sus ventajas y desventajas. Para ilustrarlo, se hace una comparación entre los resultados de una solución por fuerza bruta y una voraz en un problema del viajante. Se presentan dos tareas con exactamente la misma configuración, resueltas con sendos enfoques. Al finalizar se imprime en la pestaña de mensajes la longitud del camino recorrido y el número de iteraciones en el proceso de decisión, quedando claro que el voraz es mucho más rápido pero no da necesariamente la mejor solución, mientras que el de fuerza bruta es muy lento pero encuentra la solución óptima (ver fig. \ref{fig:modulesecondconcept}).

\begin{figure}[h]
	\begin{subfigure}{\textwidth}
		\begin{center}
			\includegraphics[width=0.7\textwidth]{images/salesmantaskgreedy}
		\end{center}
		\subcaption{Orden de recogida en la primera tarea (solución voraz).\\
			Iteraciones durante la búsqueda: 5. Longitud de la solución: 13}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\begin{center}
			\includegraphics[width=0.7\textwidth]{images/salesmantaskbrute}
		\end{center}
		\subcaption{Orden de recogida en la segunda tarea (solución de fuerza bruta).\\
			Iteraciones durante la búsqueda: 32. Longitud de la solución: 10}
	\end{subfigure}
	\caption{Comparación de resultados entre enfoque voraz y de fuerza bruta.}
	\label{fig:modulesecondconcept}
\end{figure}

\end{document}