\documentclass[../main.tex]{subfiles}

\begin{document}

\subsection{Motivación}

La \emph{alfabetización digital} se define en el Marco Global de Alfabetización Digital (DLGF) de la UNESCO \parencite[p. 6]{law2018global} de la siguiente manera:

\begin{quote}
	Digital  literacy  is  the  ability  to  access,  manage,  understand,  integrate,  communicate,  evaluate  and  create 
	information safely and appropriately through digital technologies for employment, decent jobs and 
	entrepreneurship.  It  includes  competences  that  are  variously  referred  to  as  computer  literacy,  ICT  literacy, 
	information literacy and media literacy.\footnote{La alfabetización digital es la capacidad de acceder, gestionar, comprender, integrar, comunicar, evaluar y crear 
	información de forma segura y adecuada a través de las tecnologías digitales para el empleo, el trabajo decente y el 
	el espíritu empresarial.  Incluye competencias que se denominan de diversas maneras: alfabetización informática, alfabetización en TIC 
	alfabetización informacional y alfabetización mediática.Traducción realizada con la versión gratuita del traductor www.DeepL.com/Translator .}
\end{quote}

El término Inteligencia Artificial (\textquote{Artificial Intelligence}) no aparece en ningún momento en el DLGF, a pesar de que su presencia en la sociedad no hace sino crecer y de que sus riesgos y dilemas éticos \parencite{dignum2018ethics} son tan relevantes como otros que sí se han considerado. Por estos motivos, ya existe también un interés en educar a los ciudadanos en el concepto y las posibilidades de la IA; de realizar una \emph{alfabetización en IA}. \textcite{long2020ai} realizan un estudio para su conceptualización y para establecer un marco de referencia tanto para desarrolladores como para educadores. La definición que proponen para la alfabetización en IA es:

\begin{quote}
	A set of competences that enables individuals to critically evaluate AI technologies; communicate and collaborate effectively with AI; and use AI as a tool online, at home, and in the workplace \footnote{Un conjunto de competencias que permite a las personas evaluar de forma crítica las tecnologías de la IA; comunicarse y colaborar de forma eficaz con la IA; y utilizar la IA como herramienta en línea, en casa y en el lugar de trabajo. Traducción realizada con la versión gratuita del traductor www.DeepL.com/Translator}.
\end{quote}

Para responder a ambas necesidades (alfabetización digital y alfabetización en IA), en España existe el Instituto Nacional de Tecnologías Educativas y Formación del Profesorado (INTEF), la unidad del Ministerio de Educación que se encarga de la integración de las TIC en la educación preuniversitaria \parencite{intefquienessomos} y que incluye entre sus proyectos la Escuela de Pensamiento computacional e Inteligencia Artificial \parencite{intefescuelapcia}.

Una de las varias maneras de abordar la alfabetización en IA es a través de la enseñanza de la programación de ordenadores (en adelante sólo programación). En los últimos años, como parte de la alfabetización digital y con la creciente demanda de profesionales en informática, han aparecido a nivel internacional iniciativas para incluir la programación y el pensamiento computacional en el currículo escolar \parencite{ottestad2018information}. En España esto se concreta en propuestas impulsadas por la Conferencia de Directores y Decanos de Ingeniería Informática (CODDII) y la Asociación de Enseñantes Universitarios en Informática (AENUI) \parencite{llorens2017ensenanza}, que se suman a las iniciativas que propiciarán la inclusión de los conceptos de IA en la enseñanza preuniversitaria.

Tras una búsqueda exhaustiva de herramientas para la enseñanza de programación para niños, queda claro que prácticamente la totalidad de las propuestas son plataformas en línea. Sólo algunas cuentan con contenido fuera de línea, que suele representar un pequeño porcentaje de su contenido total. Cabe mencionar algunas de las más relevantes, como \textit{Scratch}, \textit{CodeMonkey} y \textit{Code.org}. La popularidad de las aplicaciones web como herramientas educativas se debe a que facilitan el acceso a recursos actualizados desde cualquier dispositivo con conexión a Internet. Además, en el caso concreto de la programación, evitan a los centros y a los alumnos tener que instalar software de desarrollo (compiladores, intérpretes, depuradores...) al ofrecerse como servicio SaaS (Software as a Service).

Por lo tanto, es lógico concluir que la alfabetización en IA se producirá en su mayor medida a través de este tipo de plataformas en línea y que para participar en ella es necesario competir en ese mismo medio.

\subsection{Objetivos}
\subsubsection{Definir el dominio de aprendizaje}
El objetivo de este trabajo es explorar las posibilidades de enseñanza de algoritmos de IA a estudiantes preuniversitarios. El primer reto es delimitar qué se pretende enseñar, porque la etiqueta IA abarca un conjunto difuso de técnicas, que cambia con el tiempo; en palabras de \textcite{Hofstadter1979}, \textquote{IA es todo aquello que todavía no ha sido concretado}.

\textcite{launchbury2017darpa} realiza una distinción en tres olas de IA: 1) una primera ola de técnicas de \emph{conocimiento manufacturado}, también llamada en la bibliografía IA clásica, simbólica o tradicional, 2) una segunda ola de técnicas de \emph{aprendizaje estadístico}, también llamada IA moderna -que incluye la IA bioinspirada-, y 3) una futura ola de técnicas de \emph{adaptación contextual}. Será necesario realizar un análisis sobre los algoritmos pertenecientes a los dos primeros grupos, para seleccionar aquellos con mayor potencial didáctico. Esta selección no es intuitiva, porque a pesar de que las técnicas clásicas son una continuación natural del aprendizaje de la algoritmia necesario durante el aprendizaje de la programación, también pueden volverse más complejas y difíciles de ilustrar que algunas técnicas modernas.

\subsubsection{Implementar e ilustrar los algoritmos en una plataforma real}
Para enseñar un algoritmo, es conveniente no sólo describirlo, sino ilustrarlo. Por eso, incluimos entre los objetivos de este trabajo la implementación de los algoritmos que se quieran enseñar y de las utilidades necesarias para representar gráficamente el proceso de cómputo que los estudiantes deban comprender. La implementación se realizará en una plataforma existente para la enseñanza de la programación, de manera que, por lo menos, se pueda plantear la posibilidad de realizar pruebas orientadas a casos de uso reales.

\subsection{Tecnologías usadas y metodología de trabajo}

\begin{itemize}
\item La plataforma de enseñanza en línea elegida es ToolboX.Academy \footnote{https://toolbox.academy} (TXAC), desarrollada por el Departamento de Lenguajes y Ciencias de la Computación de la Universidad de Málaga \parencite{vico2019toolbox}. El frontend, donde se realizará la integración de los módulos de IA, es una aplicación web programada con Vue.js \footnote{Vue.js es un framework basado en Node.js para construir interfaces de usuario. https://vuejs.org/}. Por ello, la programación de los módulos de IA se realiza en Javascript. Aún así, el prototipo de los mismos se realiza en GNU/Octave.

\item La interfaz gráfica del mundo de Roby (ver \ref{sec:txacintro}) está programada en Phaser \footnote{Phaser es un framework para desarrollo de juegos en HTML5 para escritorio y dispositivos móviles. https://phaser.io/}. La programación de las utilidades para ilustrar los algoritmos se hará usando este mismo framework.

\item Las actividades o tareas de la plataforma están especificadas en ficheros JSON, almacenados en un repositorio aparte. Para su procesado automático, se utilizan scripts de Bash y la utilidad JQ \footnote{JQ es un procesador de ficheros en formato JSON para línea de comandos. https://stedolan.github.io/jq/}.

\item Para la grabación de los vídeos de demostración de las tareas de IA diseñadas, se utilizan scripts de Bash y la utilidad xdotool \footnote{Xdotool es una herramienta que usa librerías del sistema gestor de ventanas de algunos entornos de escritorios de Linux para simular entrada de ratón y teclado. https://www.semicomplete.com/projects/xdotool/}.

\item El control de versiones se realiza con Git. Tanto el repositorio de la aplicación como el del contenido están alojados en Bitbucket \footnote{Bitbucket es un servidor Git con una interfaz web y un sistema de usuarios y permisos sobre repositorios. https://bitbucket.org}.

\item La colaboración con el resto del equipo se ha coordinado a través de Trello \footnote{Trello es una aplicación web para organizar equipos mediante listas de tareas pendientes. https://trello.com}, utilizando una metodología ágil (Kanban) donde se realizan reuniones periódicas para comprobar el progreso del proyecto y se usan tarjetas para representar las tareas pendientes para completar los objetivos marcados.

\end{itemize}


\subsection{Estructura del documento}

En la sección de Introducción (\ref{sec:intro}) del trabajo se plantea la motivación y los objetivos del trabajo y se presentan las herramientas que se usan.

En la sección de Desarrollo (\ref{sec:desarrollo}), se hace una presentación de la plataforma ToolboX.Academy y un análisis formal en base al cual se deciden los problemas que se pueden representar en ella y qué modificaciones previas le hacen falta. A continuación, de entre los problemas estudiados, se seleccionan uno o varios para los que se implementan las soluciones de IA. Sin entrar en detalles del código, se explican las librerías escritas con las soluciones y las utilidades para ilustrarlas. Esta sección termina con una explicación de las tareas diseñadas para la enseñanza de estos algoritmos.

En la sección de Conclusiones y Líneas Futuras (\ref{sec:conclusiones}) se cierra el trabajo con un breve repaso del análisis realizado sobre los problemas de IA y se plantea cómo continuar el desarrollo del módulo de IA.

\end{document}


